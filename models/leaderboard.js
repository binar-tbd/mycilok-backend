"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class Leaderboard extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      const { UserProfile, Games } = models;
      this.belongsTo(UserProfile, { foreignKey: "player_id", as: "user_profile" });
      this.belongsTo(Games, { foreignKey: "game_id", as: "game" });
    }
  }
  Leaderboard.init(
    {
      uuid: {
        allowNull: false,
        primaryKey: true,
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
      },
      player_id: {
        type: DataTypes.UUID,
        allowNull: false,
        references: {
          model: "user_profiles",
          key: "uuid",
          onDelete: "cascade",
          onUpdate: "cascade",
        },
      },
      game_id: {
        type: DataTypes.UUID,
        allowNull: false,
        references: {
          model: "games",
          key: "uuid",
          onDelete: "cascade",
          onUpdate: "cascade",
        },
      },
      score: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
    },
    {
      sequelize,
      modelName: "Leaderboard",
      tableName: "leaderboards",
    }
  );
  return Leaderboard;
};
