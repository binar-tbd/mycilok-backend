/* eslint-disable no-undef */
const supertest = require("supertest");
const app = require("../app");

const request = supertest(app);

describe("getUserProfileByUserId function", () => {
  it("should failed get user profile by id when user id not found", async () => {
    res = await request.get("/api/v1/user-profile/qwertyuiop");
    expect(res.statusCode).toBe(404);

    expect(res.body).toHaveProperty("status");
    expect(res.body).toHaveProperty("message");
    expect(res.body).toHaveProperty("data");

    expect(res.body.status).toBe(false);
    expect(res.body.message).toBe("User Profile not found.");
    expect(res.body.data).toBe(null);
  });
});
