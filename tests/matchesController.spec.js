/* eslint-disable no-undef */
const supertest = require("supertest");
const uuidv4 = require("uuid").v4;
const app = require("../app");

const request = supertest(app);

describe("createMatch function", () => {
  it("should failed create match when player id not found", async () => {
    res = await request
      .post("/api/v1/matches")
      .send({
        player_id: uuidv4(),
        game_id: "617a8eb6-b078-4cb4-9eba-461a071e0515",
        score: 3,
      });
    expect(res.statusCode).toBe(404);

    expect(res.body).toHaveProperty("status");
    expect(res.body).toHaveProperty("message");
    expect(res.body).toHaveProperty("data");

    expect(res.body.status).toBe(false);
    expect(res.body.message).toBe("User not found.");
    expect(res.body.data).toBe(null);
  });
});
