/* eslint-disable no-undef */
const supertest = require("supertest");
const uuidv4 = require("uuid").v4;
const app = require("../app");

const request = supertest(app);
const helper = require("../helpers/testHelper");

describe("getAllGames function", () => {
  it("should successfully get all games", async () => {
    const res = await request.get("/api/v1/games");
    expect(res.statusCode).toBe(200);

    expect(res.body).toHaveProperty("status");
    expect(res.body).toHaveProperty("message");
    expect(res.body).toHaveProperty("data");

    expect(res.body.status).toBe(true);
    expect(res.body.message).toBe("success get All Games");
    expect(Array.isArray(res.body.data)).toBe(true);
    expect(res.body.data[0]).not.toHaveProperty("is_played");
  });

  it("should successfully get all games when send user_id by req.query", async () => {
    const user = await helper.createUserProfile();

    const res = await request.get("/api/v1/games").query({ user_id: user.uuid });
    expect(res.statusCode).toBe(200);

    expect(res.body).toHaveProperty("status");
    expect(res.body).toHaveProperty("message");
    expect(res.body).toHaveProperty("data");

    expect(res.body.status).toBe(true);
    expect(res.body.message).toBe("success get All Games");
    expect(Array.isArray(res.body.data)).toBe(true);
    expect(res.body.data[0]).toHaveProperty("is_played");
    await helper.deleteUserProfile(user);
  });

  it("should failed get all games when user_id not found", async () => {
    const res = await request.get("/api/v1/games").query({ user_id: "617a8eb6-b078-4cb4-9eba-461a071e0516" });
    expect(res.statusCode).toBe(404);

    expect(res.body).toHaveProperty("status");
    expect(res.body).toHaveProperty("message");
    expect(res.body).toHaveProperty("data");

    expect(res.body.status).toBe(false);
    expect(res.body.message).toBe("user profile not found.");
    expect(res.body.data).toBe(null);
  });
});

describe("getGamesById function", () => {
  it("should failed get game when id not found", async () => {
    const id = uuidv4();
    const res = await request.get(`/api/v1/games/${id}`);
    expect(res.statusCode).toBe(404);

    expect(res.body).toHaveProperty("status");
    expect(res.body).toHaveProperty("message");
    expect(res.body).toHaveProperty("data");

    expect(res.body.status).toBe(false);
    expect(res.body.message).toBe("Games not found.");
    expect(res.body.data).toBe(null);
  });
});
