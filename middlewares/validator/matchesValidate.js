const { check, validationResult } = require("express-validator");
const apiResponse = require("../../responses/api");

const rules = [
  check("player_id").trim().notEmpty().withMessage("Player id must be exists."),
  check("game_id").trim().notEmpty().withMessage("Game id cannot be empty."),
  check("score")
    .trim()
    .notEmpty()
    .withMessage("Score must be exists.")
    .isNumeric()
    .withMessage("Score must be a number."),
];

const validateMatch = [
  rules,
  // eslint-disable-next-line consistent-return
  (req, res, next) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      return apiResponse.failed(res, 422, "Unprocessable Entity", {
        errors: errors.array(),
      });
    }
    next();
  },
];

module.exports = validateMatch;
