const userProfile = require("./userProfileValidate");
const match = require("./matchesValidate");

module.exports = { userProfile, match };
