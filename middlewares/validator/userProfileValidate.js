const { check, validationResult } = require("express-validator");
const apiResponse = require("../../responses/api");

const rules = [
  check("email").trim().isEmail().withMessage("Invalid email."),
  // .normalizeEmail(),

  check("full_name")
    .trim()
    .isString()
    .withMessage("Full name should be string."),

  check("birthday").trim().isDate().withMessage("Birthday should be date"),

  check("bio").trim().isString().withMessage("Bio should be string."),
];

const validateUserProfile = [
  // Rules
  rules,
  // Response
  // eslint-disable-next-line consistent-return
  (req, res, next) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      return apiResponse.failed(res, 422, "Unprocessable Entity", {
        errors: errors.array(),
      });
    }
    next();
  },
];

module.exports = validateUserProfile;
