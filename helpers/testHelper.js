const { UserProfile, Games } = require("../models");

const createUserProfile = async () => {
  try {
    const user = await UserProfile.create({
      user_id: "Q5e0qpAkNKPSvD0pF9pWJJdr2kU2",
      full_name: "ilham",
      email: "ilham_keren1997@yahoo.co.id",
      birthday: "2000-01-01",
    });
    return user;
  } catch (error) {
    console.log("error:", error.message);
    return null;
  }
};

const deleteUserProfile = async (user) => {
  try {
    await user.destroy();
    return true;
  } catch (error) {
    console.log("error:", error.message);
    return false;
  }
};

const createGame = async () => {
  try {
    const game = await Games.create({
      name: "The New Assasin Creed",
      detail: "Some detail about assasin creed.",
      image: "https://wallpapercave.com/wp/wp2928790.jpg",
    });
    return game;
  } catch (error) {
    console.log("error:", error.message);
    return false;
  }
};

const deleteGame = async (game) => {
  try {
    await game.destroy();
    return true;
  } catch (error) {
    console.log("error:", error.message);
    return false;
  }
};

module.exports = {
  createUserProfile,
  deleteUserProfile,
  createGame,
  deleteGame,
};
