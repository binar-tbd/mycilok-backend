const { UserProfile } = require("../models");
const serviceResponse = require("../responses/service");

const get = async (field) => {
  try {
    const user = await UserProfile.findOne({
      where: field,
    });

    if (!user) return serviceResponse.failed(null, "user profile not found.");

    return serviceResponse.success(user, "Successfully get user profile.");
  } catch (error) {
    console.log("error.message", error.message);
    return serviceResponse.failed(null, error.message);
  }
};

module.exports = { get };
