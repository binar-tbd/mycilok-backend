const { Leaderboard } = require("../models");
const serviceResponse = require("../responses/service");

const get = async (field) => {
  try {
    const leaderboard = await Leaderboard.findOne({
      where: field,
    });

    return serviceResponse.success(
      leaderboard,
      "Successfully get leaderboard.",
    );
  } catch (error) {
    console.log("error.message", error.message);
    return serviceResponse.failed(null, error.message);
  }
};

const getAll = async (field, attributes) => {
  try {
    const leaderboards = await Leaderboard.findAll({
      where: field,
      attributes,
    });

    return serviceResponse.success(
      leaderboards,
      "Successfully get leaderboards.",
    );
  } catch (error) {
    console.log("error.message", error.message);
    return serviceResponse.failed(null, error.message);
  }
};

module.exports = { get, getAll };
