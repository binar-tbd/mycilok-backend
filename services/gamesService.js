const { Games } = require("../models");
const serviceResponse = require("../responses/service");

const get = async (field) => {
  try {
    const game = await Games.findOne({
      where: field,
    });

    return serviceResponse.success(game, "Successfully get game.");
  } catch (error) {
    console.log("error.message", error.message);
    return serviceResponse.failed(null, error.message);
  }
};

module.exports = { get };
