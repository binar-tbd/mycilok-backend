const express = require("express");

const router = express.Router();
const userProfile = require("../controllers/userProfileController");
const Games = require("../controllers/gamesController");
const matches = require("../controllers/matchesController");
const validate = require("../middlewares/validator");

router.use(express.json());

// User Profiles
router.get("/user-profile/:user_id", userProfile.getUserProfileByUserId);
router.post(
  "/user-profile/:user_id",
  validate.userProfile,
  userProfile.createUserProfile,
);

// Games
router.get("/games/:id", Games.getGamesById);
router.get("/games", Games.getAllGames);
router.post("/games", Games.createGames);

// Matches
router.post("/matches", validate.match, matches.createMatch);

module.exports = router;
