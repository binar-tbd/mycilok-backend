/* eslint-disable camelcase */
const { UserProfile } = require("../models");
const apiResponse = require("../responses/api");
const leaderboardService = require("../services/leaderboardService");
const gamesService = require("../services/gamesService");

const getUserProfileByUserId = async (req, res) => {
  try {
    const userId = req.params.user_id;

    const userProfile = await UserProfile.findOne({
      where: { user_id: userId },
    });

    if (userProfile) {
      const game = await gamesService.get({
        name: "RPS",
      });

      const dataLeaderBoard = await leaderboardService.get({
        player_id: userProfile.uuid,
        game_id: game.data.uuid,
      });

      const tempUserGames = await leaderboardService.getAll(
        { player_id: userProfile.uuid },
        ["game_id"],
      );
      const userGames = [];
      if (tempUserGames.status) {
        tempUserGames.data.forEach((item) => {
          userGames.push(item.dataValues.game_id);
        });
      }

      if (dataLeaderBoard.status && tempUserGames.status) {
        return apiResponse.success(
          res,
          200,
          "Successfully get user profile by user id.",
          {
            ...userProfile.dataValues,
            score: dataLeaderBoard.data?.score || 0,
            user_games: userGames,
          },
        );
      }
    }

    return apiResponse.failed(res, 404, "User Profile not found.", null);
  } catch (error) {
    console.log("error.message", error.message);
    return apiResponse.failed(res, 500, error.message, null);
  }
};

const createUserProfile = async (req, res) => {
  try {
    const {
      full_name, birthday, bio, email, photo_url,
    } = req.body;
    const { user_id } = req.params;

    let userProfile = await UserProfile.findOne({
      where: { user_id },
    });

    if (userProfile) {
      await UserProfile.update(
        {
          full_name,
          birthday,
          bio,
          email,
          user_id,
          photo_url,
        },
        { where: { user_id } },
      );
    }

    if (!userProfile) {
      userProfile = await UserProfile.create({
        full_name,
        birthday,
        bio,
        email,
        user_id,
      });
    }

    return apiResponse.success(
      res,
      201,
      "Successfully create user profile",
      userProfile,
    );
  } catch (error) {
    console.log("error.message", error.message);
    return apiResponse.failed(res, 500, error.message, null);
  }
};

module.exports = {
  getUserProfileByUserId,
  createUserProfile,
};
