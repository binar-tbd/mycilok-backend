const {
  Match, UserProfile, Games, Leaderboard,
} = require("../models");
const apiResponse = require("../responses/api");

const createMatch = async (req, res) => {
  try {
    // eslint-disable-next-line camelcase
    const { player_id, game_id } = req.body;
    const score = parseFloat(req.body.score);

    const player = await UserProfile.findOne({ where: { uuid: player_id } });
    if (!player) {
      return apiResponse.failed(res, 404, "User not found.", null);
    }

    const game = await Games.findOne({ where: { uuid: game_id } });
    if (!game) {
      return apiResponse.failed(res, 404, "Game not found.", null);
    }

    let match = await Match.create({ player_id, game_id, score });

    const leaderboard = await Leaderboard.findOne({
      where: {
        player_id,
        game_id,
      },
    });

    if (leaderboard) {
      let tempScore = parseFloat(leaderboard.score);
      await Leaderboard.update(
        {
          player_id,
          game_id,
          score: (tempScore += score),
        },
        { where: { player_id, game_id } },
      );
    }

    if (!leaderboard) {
      await Leaderboard.create({
        player_id,
        game_id,
        score,
      });
    }

    match = await Match.findOne(
      {
        include: ["player", "game"],
      },
      { where: { uuid: match.uuid } },
    );

    return apiResponse.success(res, 201, "Successfully create match.", match);
  } catch (error) {
    console.log(error.message);
    return apiResponse.failed(res, 500, error.message, null);
  }
};

module.exports = { createMatch };
