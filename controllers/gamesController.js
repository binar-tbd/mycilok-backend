/* eslint-disable consistent-return */
const { QueryTypes } = require("sequelize");
const { Games, Leaderboard, sequelize } = require("../models");
const apiResponse = require("../responses/api");
const userProfileService = require("../services/userProfileService");

const getGamesById = async (req, res) => {
  try {
    const Id = req.params.id;

    const games = await Games.findOne({
      where: { uuid: Id },
      include: [
        {
          model: Leaderboard,
          as: "leaderboards",
          include: ["user_profile"],
        },
      ],
      order: [[{ model: Leaderboard, as: "leaderboards" }, "score", "desc"]],
    });

    if (games) {
      return apiResponse.success(
        res,
        200,
        "Successfully get Games by id.",
        games,
      );
    }

    return apiResponse.failed(res, 404, "Games not found.", null);
  } catch (error) {
    console.log("error.message", error.message);
    apiResponse.failed(res, 500, error.message, null);
  }
};

const getAllGames = async (req, res) => {
  try {
    const userId = req.query.user_id || null;

    let games = [];

    if (userId) {
      const user = await userProfileService.get({ uuid: userId });
      if (!user.status) {
        return apiResponse.failed(res, 404, "user profile not found.", null);
      }

      games = await sequelize.query(
        `select g.*, \
        case when l.game_id is not null then true \
        else false end as is_played \
        from games g \
        left join ( \
          select l.game_id from leaderboards l \
          where l.player_id = '${userId}' \
        ) as l on l.game_id = g.uuid`,
        { type: QueryTypes.SELECT },
      );
    } else {
      games = await Games.findAll();
    }

    apiResponse.success(res, 200, "success get All Games", games);
  } catch (error) {
    console.log("error.message", error.message);
    apiResponse.failed(res, 500, error.message, null);
  }
};

const createGames = async (req, res) => {
  try {
    const { name, detail, image } = req.body;
    let putGame;
    let newGame = null;
    if (req.body.uuid) {
      putGame = await Games.update(
        {
          name,
          detail,
          image,
        },
        { where: { uuid: req.body.uuid } },
      );
    } else {
      newGame = await Games.create({
        name,
        detail,
        image,
      });
    }

    return apiResponse.success(
      res,
      201,
      "Successfully create/update Game",
      putGame ?? newGame,
    );
  } catch (error) {
    console.log("error.message", error.message);
    apiResponse.failed(res, 500, error.message, null);
  }
};

module.exports = {
  getGamesById,
  createGames,
  getAllGames,
};
