"use strict";
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable("matches", {
      uuid: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
      },
      player_id: {
        type: Sequelize.UUID,
        allowNull: false,
        references: {
          model: "user_profiles",
          key: "uuid",
          onDelete: "cascade",
          onUpdate: "cascade",
        },
      },
      game_id: {
        type: Sequelize.UUID,
        allowNull: false,
        references: {
          model: "games",
          key: "uuid",
          onDelete: "cascade",
          onUpdate: "cascade",
        },
      },
      score: {
        type: Sequelize.INTEGER,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable("matches");
  },
};
