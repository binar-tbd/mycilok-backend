const express = require("express");

const app = express();
const cors = require("cors");

app.use(cors());
app.use(express.urlencoded({ extended: true }));
app.use(express.json());

const router = require("./routes/router");

app.use("/api/v1", router);

// app.use((error, req, res) => {
//   const status = error.errorStatus || 500;
//   const { message } = error;
//   const { data } = error;
//   console.log(error);

//   res.status(status).json({ message, data });
// });

module.exports = app;
