"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */
    await queryInterface.bulkInsert(
      "games",
      [
        {
          uuid: "617a8eb6-b078-4cb4-9eba-461a071e0515",
          name: "RPS",
          detail: "this is RPS GAME this is RPS GAME this is RPS GAME",
          image:
            "https://static.vecteezy.com/system/resources/previews/000/693/121/non_2x/paw-sign-of-rock-paper-scissors-game-vector.png",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          uuid: "617a8eb6-b078-4cb4-9eba-461a071e0519",
          name: "Assasin Creed",
          detail: "this is Assasin Creed Game",
          image: "https://wallpapercave.com/wp/wp2928790.jpg",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          uuid: "617a8eb6-b078-4cb4-9eba-461a071e0517",
          name: "Fight",
          detail: "this is Fight Game",
          image: "https://wallpapercave.com/wp/wp2936648.jpg",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          uuid: "617a8eb6-b078-4cb4-9eba-461a071e0513",
          name: "BattleField",
          detail: "this is BattleField Game",
          image: "https://wallpapercave.com/wp/wp2936651.jpg",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ],
      {}
    );
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  },
};
